from flask import Blueprint, request
from scripts.handler.color_finder_handler import find_color

color_finder = Blueprint('color_finder', __name__)


@color_finder.route('/', methods=['POST'])
def post_request():
    content = request.get_json()
    ans = find_color(content)
    return ans
