list_items = []


def find_color(data):
    if not isinstance(data, str):
        for k, v in data.items():
            if isinstance(v, dict):
                find_color(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for items in v:
                    if isinstance(items, dict):
                        find_color(items)
            elif isinstance(v, str):
                if k == "color":
                    list_items.append(v)
            else:
                if k == "color":
                    list_items.append(v)

    # list2 = []
    # list2 = list(set(list_items))
    # listToStr = ' '.join([str(list2) for elem in list2])
    return {"data": list(set(list_items))}
