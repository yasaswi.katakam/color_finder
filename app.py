from flask import Flask
from scripts.service.color_finder_service import color_finder

app = Flask('__name__')
app.register_blueprint(color_finder)
if __name__ == '__main__':
    app.run(port=8008)
